package pl.plodz.mstokfisz.Operators;

public class Divider implements IMathOperation  {
  public double performOperation(double x, double y) throws ArithmeticException {
    if (y == 0) {
      throw new ArithmeticException("Division by 0");
    }
    return x / y;
  }
}
