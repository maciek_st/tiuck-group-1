package pl.plodz.mstokfisz.Operators;

public class Summer implements IMathOperation {
  public double performOperation(double x, double y) {
    return x + y;
  }
}
