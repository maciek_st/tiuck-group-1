package pl.plodz.mstokfisz.Operators;

public class Multiplier implements IMathOperation {
  public double performOperation(double x, double y) {
    return x * y;
  }
}
