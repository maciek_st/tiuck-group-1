package pl.plodz.mstokfisz.Operators;

public interface IMathOperation {
  double performOperation(double x, double y);
}
