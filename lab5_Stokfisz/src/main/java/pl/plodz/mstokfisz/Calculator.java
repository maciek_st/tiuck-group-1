package pl.plodz.mstokfisz;

import org.apache.log4j.Logger;
import pl.plodz.mstokfisz.Operators.*;

public class Calculator {

  static final Logger logger = Logger.getLogger(Calculator.class);
  public static void main(String[] args) {
    if (args == null || args.length != 3 || args[1].length() != 1) {
      logger.error("Wrong input parameters");
      logger.error("{firstNumber} {operator} {secondNumber}");
    } else {
      IMathOperation divider = new Divider();
      IMathOperation multiplier = new Multiplier();
      IMathOperation summer = new Summer();
      IMathOperation subtractor = new Subtractor();
      double x = Double.parseDouble(args[0]);
      char op = args[1].charAt(0);
      double y = Double.parseDouble(args[2]);
      double result;
      switch (op) {
        case '+':
          result = summer.performOperation(x, y);
          break;
        case '-':
          result = subtractor.performOperation(x, y);
          break;
        case '*':
          result = multiplier.performOperation(x, y);
          break;
        case '/':
          result = divider.performOperation(x, y);
          break;
        default:
          throw new UnsupportedOperationException("Operation not recognized");
      }
      System.out.println("Result: " + result);
    }
  }
}
