import pl.plodz.mstokfisz.Operators.Divider;
import pl.plodz.mstokfisz.Operators.Multiplier;
import pl.plodz.mstokfisz.Operators.Subtractor;
import pl.plodz.mstokfisz.Operators.Summer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import pl.plodz.mstokfisz.Calculator;

import java.io.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {
    private Summer summer;
    private Subtractor subtractor;
    private Multiplier multiplier;
    private Divider divider;
    private double firstNum;
    private double secondNum;
    private double result;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void initTest() {
        summer = new Summer();
        subtractor = new Subtractor();
        multiplier = new Multiplier();
        divider = new Divider();
        firstNum = 15.5;
        secondNum = 5.5;
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testCalculatorSuccess() {
        Calculator.main(new String[]{"10.0", "/", "5.0"});
        assertThat(outContent.toString(), containsString("2"));
        Calculator.main(new String[]{"10.0", "*", "5.0"});
        assertThat(outContent.toString(), containsString("50"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCalculatorFailures() {
        Calculator.main(new String[]{});
        Calculator.main(new String[]{"10.0", "d", "5.0"});
    }

    @Test
    public void testSummer() {
        Calculator.main(new String[]{Double.toString(firstNum), "+", Double.toString(secondNum)});
        assertThat(outContent.toString(), containsString("Result: 21.0"));
    }

    @Test
    public void testSubtractor() {
        Calculator.main(new String[]{Double.toString(firstNum), "-", Double.toString(secondNum)});
        assertThat(outContent.toString(), containsString("Result: 10.0"));
    }

    @Test
    public void testMultiplier() {
        Calculator.main(new String[]{Double.toString(firstNum), "*", Double.toString(secondNum)});
        assertThat(outContent.toString(), containsString("Result: 85.25"));
    }

    @Test
    public void testDivider() {
        Calculator.main(new String[]{Double.toString(firstNum), "/", Double.toString(secondNum)});
        assertThat(outContent.toString(), containsString("Result: 2.818181818"));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        Calculator.main(new String[]{Double.toString(firstNum), "/", "0"});
    }

    @After
    public void restoreOriginalStream() {
        System.setOut(originalOut);
    }
}
