import {fsSource, indices, positions, textureCoordinates, vertexNormals, vsSource} from './constants.js';
import {Shader} from './shader.js';
import {ShaderProgram} from './shaderProgram.js';
import {Buffer} from './buffer.js';
import {Texture} from './texture.js';


let cubeRotation = 0.0;

main();

function main() {
    const canvas = document.querySelector('#glcanvas');
    const gl = canvas.getContext('webgl');

    // If we don't have a GL context, give up now
    if (!gl) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        return;
    }

    // Initialize a shader program; this is where all the lighting
    // for the vertices and so forth is established.
    const vertexShader = new Shader(gl, gl.VERTEX_SHADER, vsSource);
    const fragmentShader = new Shader(gl, gl.FRAGMENT_SHADER, fsSource);
    const shaders = [vertexShader.get(), fragmentShader.get()];
    const shaderProgram = new ShaderProgram(gl, shaders);
    const shaderProgramInstance = shaderProgram.get();


    // Collect all the info needed to use the shader program.
    // Look up which attributes our shader program is using
    // for aVertexPosition, aVertexNormal, aTextureCoord,
    // and look up uniform locations.
    const programInfo = {
        program: shaderProgramInstance,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgramInstance, 'aVertexPosition'),
            vertexNormal: gl.getAttribLocation(shaderProgramInstance, 'aVertexNormal'),
            textureCoord: gl.getAttribLocation(shaderProgramInstance, 'aTextureCoord'),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgramInstance, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgramInstance, 'uModelViewMatrix'),
            normalMatrix: gl.getUniformLocation(shaderProgramInstance, 'uNormalMatrix'),
            uSampler: gl.getUniformLocation(shaderProgramInstance, 'uSampler'),
        },
    };

    // Here's where we call the routine that builds all the
    // objects we'll be drawing.
    const buffers = initBuffers(gl);

    const texture = loadTexture(gl, 'cubetexture.png');

    let then = 0;

    // Draw the scene repeatedly
    function render(now) {
        now *= 0.001;  // convert to seconds
        const deltaTime = now - then;
        then = now;

        drawScene(gl, programInfo, buffers, texture, deltaTime);

        requestAnimationFrame(render);
    }

    requestAnimationFrame(render);
}

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just
// have one object -- a simple three-dimensional cube.
//
function initBuffers(gl) {

    // Create a buffer for the cube's vertex positions.
    const positionBuffer = new Buffer(gl, gl.ARRAY_BUFFER, new Float32Array(positions));

    // Set up the normals for the vertices, so that we can compute lighting.
    const normalBuffer = new Buffer(gl, gl.ARRAY_BUFFER, new Float32Array(vertexNormals));

    // Now set up the texture coordinates for the faces.
    const textureCoordBuffer = new Buffer(gl, gl.ARRAY_BUFFER, new Float32Array(textureCoordinates));

    // Build the element array buffer; this specifies the indices
    // into the vertex arrays for each face's vertices.
    const indexBuffer = new Buffer(gl, gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices));

    return {
        position: positionBuffer,
        normal: normalBuffer,
        textureCoord: textureCoordBuffer,
        indices: indexBuffer,
    };
}

//
// Initialize a texture and load an image.
// When the image finished loading copy it into the texture.
//
function loadTexture(gl, url) {
    const texture = new Texture(gl);
    const image = new Image();
    image.onload = function () {
        texture.onImageLoad(image);
    };
    image.src = url;

    return texture.get();
}

//
// Draw the scene.
//
function drawScene(gl, programInfo, buffers, texture, deltaTime) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

    // Clear the canvas before we start drawing on it.

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Create a perspective matrix, a special matrix that is
    // used to simulate the distortion of perspective in a camera.
    // Our field of view is 45 degrees, with a width/height
    // ratio that matches the display size of the canvas
    // and we only want to see objects between 0.1 units
    // and 100 units away from the camera.

    const fieldOfView = 45 * Math.PI / 180;   // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();

    // note: glmatrix.js always has the first argument
    // as the destination to receive the result.
    mat4.perspective(projectionMatrix,
        fieldOfView,
        aspect,
        zNear,
        zFar);

    // Set the drawing position to the "identity" point, which is
    // the center of the scene.
    const modelViewMatrix = mat4.create();

    // Now move the drawing position a bit to where we want to
    // start drawing the square.

    mat4.translate(modelViewMatrix,     // destination matrix
        modelViewMatrix,     // matrix to translate
        [-0.0, 0.0, -6.0]);  // amount to translate
    mat4.rotate(modelViewMatrix,  // destination matrix
        modelViewMatrix,  // matrix to rotate
        cubeRotation,     // amount to rotate in radians
        [0, 0, 1]);       // axis to rotate around (Z)
    mat4.rotate(modelViewMatrix,  // destination matrix
        modelViewMatrix,  // matrix to rotate
        cubeRotation * .7,// amount to rotate in radians
        [0, 1, 0]);       // axis to rotate around (X)

    const normalMatrix = mat4.create();
    mat4.invert(normalMatrix, modelViewMatrix);
    mat4.transpose(normalMatrix, normalMatrix);

    // Bind buffered data with attributes
    buffers.position.bindWithAttribute(programInfo.attribLocations.vertexPosition, 3);
    buffers.textureCoord.bindWithAttribute(programInfo.attribLocations.textureCoord, 2);
    buffers.normal.bindWithAttribute(programInfo.attribLocations.vertexNormal, 3);

    // Tell WebGL which indices to use to index the vertices
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices.get());

    // Tell WebGL to use our program when drawing

    gl.useProgram(programInfo.program);

    // Set the shader uniforms
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.normalMatrix,
        false,
        normalMatrix);

    // Specify the texture to map onto the faces.

    // Tell WebGL we want to affect texture unit 0
    gl.activeTexture(gl.TEXTURE0);

    // Bind the texture to texture unit 0
    gl.bindTexture(gl.TEXTURE_2D, texture);

    // Tell the shader we bound the texture to texture unit 0
    gl.uniform1i(programInfo.uniformLocations.uSampler, 0);

    {
        const vertexCount = 36;
        const type = gl.UNSIGNED_SHORT;
        const offset = 0;
        gl.drawElements(gl.TRIANGLES, vertexCount, type, offset);
    }

    // Update the rotation for the next draw

    cubeRotation += deltaTime;
}
