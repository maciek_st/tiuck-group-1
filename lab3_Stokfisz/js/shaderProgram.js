export class ShaderProgram {
    constructor(gl, shaders) {
        this.shaderProgram = gl.createProgram();
        shaders.forEach((shader) => {
            gl.attachShader(this.shaderProgram, shader);
        });
        gl.linkProgram(this.shaderProgram);
        if (!gl.getProgramParameter(this.shaderProgram, gl.LINK_STATUS)) {
            throw new Error('Unable to initialize the shader program: ' + gl.getProgramInfoLog(this.shaderProgram));
        }
    }

    get() {
        return this.shaderProgram;
    }
}