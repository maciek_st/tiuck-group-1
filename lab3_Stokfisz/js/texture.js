import {textureProps} from './constants.js';

function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}

export class Texture {
    constructor(gl) {
        this.gl = gl;
        this.texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        // Load default texture, when the image is not loaded
        this.loadDefault(gl);
    }

    loadDefault() {
        this.gl.texImage2D(this.gl.TEXTURE_2D, textureProps.level, this.gl.RGBA,
            textureProps.width, textureProps.height, textureProps.border, this.gl.RGBA, this.gl.UNSIGNED_BYTE,
            textureProps.pixel);
    }

    onImageLoad(image) {
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
        this.gl.texImage2D(this.gl.TEXTURE_2D, textureProps.level, this.gl.RGBA,
            this.gl.RGBA, this.gl.UNSIGNED_BYTE, image);
        // WebGL1 has different requirements for power of 2 images
        // vs non power of 2 images so check if the image is a
        // power of 2 in both dimensions.
        if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
            // Yes, it's a power of 2. Generate mips.
            this.gl.generateMipmap(this.gl.TEXTURE_2D);
        } else {
            // No, it's not a power of 2. Turn of mips and set
            // wrapping to clamp to edge
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
            this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
        }
    }

    get() {
        return this.texture;
    }
}