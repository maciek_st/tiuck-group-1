const normalize = false;
const stride = 0;
const offset = 0;
export class Buffer {
    constructor(gl, type, bufferData) {
        this.gl = gl;
        this.type = type;
        this.buffer = gl.createBuffer();
        gl.bindBuffer(type, this.buffer);
        gl.bufferData(type, bufferData, gl.STATIC_DRAW);
    }

    get() {
        return this.buffer;
    }

    // Tell WebGL how to pull out the buffered data into the attrib attribute.
    bindWithAttribute(attrib, numComponents) {
        this.gl.bindBuffer(this.type, this.buffer);
        this.gl.vertexAttribPointer(
            attrib,
            numComponents,
            this.gl.FLOAT,
            normalize,
            stride,
            offset);
        this.gl.enableVertexAttribArray(
            attrib);
    }
}