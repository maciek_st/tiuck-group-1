class TextureLoader {

    //
    // Initialize a texture and load an image.
    // When the image finished loading copy it into the texture.
    //
    loadTexture(context, url) {
        const texture = context.createTexture();
        context.bindTexture(context.TEXTURE_2D, texture);
        const self = this;

        // Because images have to be download over the internet
        // they might take a moment until they are ready.
        // Until then put a single pixel in the texture so we can
        // use it immediately. When the image has finished downloading
        // we'll update the texture with the contents of the image.
        const level = 0;
        const internalFormat = context.RGBA;
        const width = 1;
        const height = 1;
        const border = 0;
        const sourceFormat = context.RGBA;
        const sourceType = context.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
        context.texImage2D(context.TEXTURE_2D, level, internalFormat,
            width, height, border, sourceFormat, sourceType,
            pixel);

        const image = new Image();
        image.onload = function() {
            context.bindTexture(context.TEXTURE_2D, texture);
            context.texImage2D(context.TEXTURE_2D, level, internalFormat,
                sourceFormat, sourceType, image);

            // WebGL has different requirements for power of 2 images
            // vs non power of 2 images so check if the image is a
            // power of 2 in both dimensions.
            if (self.isPowerOf2(image.width) && self.isPowerOf2(image.height)) {
                // Yes, it's a power of 2. Generate mips.
                context.generateMipmap(context.TEXTURE_2D);
            } else {
                // No, it's not a power of 2. Turn of mips and set
                // wrapping to clamp to edge
                context.texParameteri(context.TEXTURE_2D, context.TEXTURE_WRAP_S, context.CLAMP_TO_EDGE);
                context.texParameteri(context.TEXTURE_2D, context.TEXTURE_WRAP_T, context.CLAMP_TO_EDGE);
                context.texParameteri(context.TEXTURE_2D, context.TEXTURE_MIN_FILTER, context.LINEAR);
            }
        };
        image.src = url;

        return texture;
    }

    isPowerOf2(value) {
        return (value & (value - 1)) === 0;
    }
}