class ShaderCompiler {

    vertexShaderSource = `
    attribute vec4 aVertexPosition;
    attribute vec3 aVertexNormal;
    attribute vec2 aTextureCoord;

    uniform mat4 uNormalMatrix;
    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying highp vec2 vTextureCoord;
    varying highp vec3 vLighting;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vTextureCoord = aTextureCoord;

      // Apply lighting effect

      highp vec3 ambientLight = vec3(0.3, 0.3, 0.3);
      highp vec3 directionalLightColor = vec3(1, 1, 1);
      highp vec3 directionalVector = normalize(vec3(0.85, 0.8, 0.75));

      highp vec4 transformedNormal = uNormalMatrix * vec4(aVertexNormal, 1.0);

      highp float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);
      vLighting = ambientLight + (directionalLightColor * directional);
    }
  `;

    fragmentShaderSource = `
    varying highp vec2 vTextureCoord;
    varying highp vec3 vLighting;

    uniform sampler2D uSampler;

    void main(void) {
      highp vec4 texelColor = texture2D(uSampler, vTextureCoord);

      gl_FragColor = vec4(texelColor.rgb * vLighting, texelColor.a);
    }
  `;

    //
    // Initialize a shader program, so WebGL knows how to draw our data
    //
    initializeShaderProgram(context) {
        const vertexShader = this.loadShader(context, context.VERTEX_SHADER, this.vertexShaderSource);
        const fragmentShader = this.loadShader(context, context.FRAGMENT_SHADER, this.fragmentShaderSource);

        // Create the shader program
        const shaderProgram = context.createProgram();
        context.attachShader(shaderProgram, vertexShader);
        context.attachShader(shaderProgram, fragmentShader);
        context.linkProgram(shaderProgram);

        // If creating the shader program failed, alert
        if (!context.getProgramParameter(shaderProgram, context.LINK_STATUS)) {
            alert('Unable to initialize the shader program: ' + context.getProgramInfoLog(shaderProgram));
            return null;
        }

        return shaderProgram;
    }

    //
    // creates a shader of the given type, uploads the source and
    // compiles it.
    //
    loadShader(context, type, source) {
        const shader = context.createShader(type);

        // Send the source to the shader object
        context.shaderSource(shader, source);

        // Compile the shader program
        context.compileShader(shader);

        // See if it compiled successfully
        if (!context.getShaderParameter(shader, context.COMPILE_STATUS)) {
            alert('An error occurred compiling the shaders: ' + context.getShaderInfoLog(shader));
            context.deleteShader(shader);
            return null;
        }

        return shader;
    }
}