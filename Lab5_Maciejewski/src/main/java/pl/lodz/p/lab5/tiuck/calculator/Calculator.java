package pl.lodz.p.lab5.tiuck.calculator;

import java.util.Map;
import pl.lodz.p.lab5.tiuck.calculator.operators.IOperator;
import pl.lodz.p.lab5.tiuck.exceptions.WrongSymbolException;

public class Calculator implements ICalculator {

  Map<String, IOperator> operations;

  public Calculator(Map<String, IOperator> supportedOperations) {
    this.operations = supportedOperations;
  }

  @Override
  public double calculate(double number1, double number2, String operator)
      throws WrongSymbolException {
    if (!this.operations.containsKey(operator)) {
      throw new WrongSymbolException("Unsupported operation: " + operator);
    }

    return operations.get(operator).performCalculation(number1, number2);
  }
}
