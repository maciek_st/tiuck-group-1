package pl.lodz.p.lab5.tiuck.calculator.operators;

import pl.lodz.p.lab5.tiuck.exceptions.WrongSymbolException;

public interface IOperator {
  double performCalculation(double number1, double number2) throws WrongSymbolException;
}
