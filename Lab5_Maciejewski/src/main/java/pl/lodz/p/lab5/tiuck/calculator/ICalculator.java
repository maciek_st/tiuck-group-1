package pl.lodz.p.lab5.tiuck.calculator;

import pl.lodz.p.lab5.tiuck.exceptions.WrongSymbolException;

public interface ICalculator {
  double calculate(double number1, double number2, String operand) throws WrongSymbolException;
}
