package pl.lodz.p.lab5.tiuck.exceptions;

public class WrongSymbolException extends Exception {

  public WrongSymbolException(String s) {
    super(s);
  }
}
