package pl.lodz.p.lab5.tiuck.parser;

import java.util.ArrayList;
import java.util.List;

public class ArgumentParser implements IArgumentParser {

  private List<String> parseErrors;

  public ArgumentParser() {
    parseErrors = new ArrayList<>(5);
  }

  @Override
  public boolean parse(String[] arguments) {
    boolean result = true;

    if (arguments.length != 3) {
      parseErrors.add("Error: Exactly 3 arguments are required");
      result = false;
    }

    if (result) {
      try {
        Double.parseDouble(arguments[0]);
      } catch (Exception e) {
        parseErrors.add(
            "Error: Argument: " + arguments[0] + " " + "does not contain a parsable number");
        result = false;
      }

      try {
        Double.parseDouble(arguments[2]);
      } catch (Exception e) {
        parseErrors.add(
            "Error: Argument: " + arguments[2] + " " + "does not contain a parsable number");
        result = false;
      }
    }
    return result;
  }

  @Override
  public List<String> getParseErrors() {
    return parseErrors;
  }
}
