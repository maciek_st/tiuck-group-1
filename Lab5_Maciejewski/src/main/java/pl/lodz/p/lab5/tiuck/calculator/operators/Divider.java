package pl.lodz.p.lab5.tiuck.calculator.operators;

import pl.lodz.p.lab5.tiuck.exceptions.WrongSymbolException;

public class Divider implements IOperator {
  @Override
  public double performCalculation(double number1, double number2) throws WrongSymbolException {
    if (number2 == 0.0) {
      throw new WrongSymbolException("You can't divide by 0!");
    }
    return number1 / number2;
  }
}
