package pl.lodz.p.lab5.tiuck.calculator.operators;

import pl.lodz.p.lab5.tiuck.exceptions.WrongSymbolException;

public class Subtractor implements IOperator {
  @Override
  public double performCalculation(double number1, double number2) throws WrongSymbolException {
    return number1 - number2;
  }
}
