package pl.lodz.p.lab5.tiuck.main;

public class Main {

  public static void main(String[] args) {
    Worker manForCalculation = new Worker();
    String result = manForCalculation.performDuties(args);
    System.out.println(result);
  }
}
