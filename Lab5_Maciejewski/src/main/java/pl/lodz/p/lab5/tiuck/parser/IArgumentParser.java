package pl.lodz.p.lab5.tiuck.parser;

import java.util.List;

public interface IArgumentParser {
  boolean parse(String[] arguments);

  List<String> getParseErrors();
}
