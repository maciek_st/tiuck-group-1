package pl.lodz.p.lab5.tiuck.main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import pl.lodz.p.lab5.tiuck.calculator.Calculator;
import pl.lodz.p.lab5.tiuck.calculator.ICalculator;
import pl.lodz.p.lab5.tiuck.calculator.operators.*;
import pl.lodz.p.lab5.tiuck.parser.ArgumentParser;
import pl.lodz.p.lab5.tiuck.parser.IArgumentParser;

public class Worker {

  public String performDuties(String[] arguments) {
    String answer;
    IArgumentParser parser = new ArgumentParser();
    boolean isArgumentsOk = parser.parse(arguments);
    if (isArgumentsOk) {
      ICalculator worker = new Calculator(prepareMapForCalculator());
      try {
        double result =
            worker.calculate(
                Double.parseDouble(arguments[0]), Double.parseDouble(arguments[2]), arguments[1]);
        answer = presentResults(arguments, result);

      } catch (Exception e) {
        answer = presentException(e);
      }
    } else {
      answer = presentParseErrors(parser.getParseErrors());
    }
    return answer;
  }

  private Map<String, IOperator> prepareMapForCalculator() {
    Map<String, IOperator> result = new HashMap<>();
    result.put("+", new Summer());
    result.put("-", new Subtractor());
    result.put("*", new Multiplier());
    result.put("/", new Divider());
    return result;
  }

  private String presentResults(String[] args, double result) {
    return args[0] + " " + args[1] + " " + args[2] + " = " + result;
  }

  private String presentParseErrors(List<String> errors) {
    StringBuilder result = new StringBuilder();
    result.append("Arguments no OK" + '\n');
    for (String error : errors) {
      result.append(error + '\n');
    }
    return result.toString();
  }

  private String presentException(Exception e) {
    String result;
    result = "Exception " + e;
    return result;
  }
}
