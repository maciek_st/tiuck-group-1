import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import pl.lodz.p.lab5.tiuck.calculator.Calculator;
import pl.lodz.p.lab5.tiuck.calculator.operators.*;
import pl.lodz.p.lab5.tiuck.main.Main;
import pl.lodz.p.lab5.tiuck.parser.ArgumentParser;

@RunWith(MockitoJUnitRunner.class)
public class MainTest {

  private ArgumentParser parser;
  private Summer summer;
  private Subtractor subtractor;
  private Multiplier multiplier;
  private Divider divider;
  private Calculator calculator;
  private final PrintStream trueOut = System.out;
  private final ByteArrayOutputStream testOut = new ByteArrayOutputStream();

  @Before
  public void initTest() {
    parser = new ArgumentParser();
    summer = new Summer();
    subtractor = new Subtractor();
    multiplier = new Multiplier();
    divider = new Divider();
    Map<String, IOperator> operations = new HashMap<>();
    operations.put("+", summer);
    operations.put("-", subtractor);
    operations.put("*", multiplier);
    operations.put("/", divider);
    calculator = new Calculator(operations);
    // na czas testów przekierowujemy standardowe wyjście
    System.setOut(new PrintStream(testOut));
  }

  @After
  public void clear() {
    // przywracamy standardowe wyjście do stanu pierwotnego
    System.setOut(trueOut);
  }

  @Test
  public void testParserTrue() {
    String[] arguments = {"4.0", "+", "4.0"};
    boolean result = parser.parse(arguments);
    assertTrue(result == true);
  }

  @Test
  public void testParserFalse() {
    String[] arguments = {"4.0", "+", "MAMA"};
    boolean result = parser.parse(arguments);
    assertTrue(result == false);
  }

  @Test
  public void testParserWrongNumberOfArguments() {
    String[] arguments = {"4.0"};
    boolean result = parser.parse(arguments);
    assertTrue(result == false);
  }

  @Test
  public void testParserListOfErrors() {
    String[] arguments = {"4.0"};
    boolean result = parser.parse(arguments);
    assertTrue(parser.getParseErrors().size() == 1);
  }

  @Test
  public void testSum() throws Exception {
    double result = summer.performCalculation(4.0, 2.0);
    assertTrue(result == 6.0);
  }

  @Test
  public void testSubtract() throws Exception {
    double result = subtractor.performCalculation(8.0, -152.0);
    assertTrue(result == 160.0);
  }

  @Test
  public void testMultiply() throws Exception {
    double result = multiplier.performCalculation(9.0, 9.5);
    assertTrue(result == 85.5);
  }

  @Test
  public void testDivide() throws Exception {
    double result = divider.performCalculation(4.0, 2.0);
    assertTrue(result == 2.0);
  }

  @Test(expected = Exception.class)
  public void testDivideByZero() throws Exception {
    divider.performCalculation(2.0, 0);
  }

  @Test
  public void testCalculator() throws Exception {
    double result = calculator.calculate(4.0, 5.0, "*");
    assertTrue(result == 20.0);
  }

  @Test(expected = Exception.class)
  public void testCalculatorException() throws Exception {
    double result = calculator.calculate(4.0, 5.0, "y");
  }

  @Test
  public void testMainWrongArgumentsNumber() {
    String[] arguments = {"2.0", "+"};
    Main.main(arguments);
    assertThat(testOut.toString(), containsString("Arguments no OK"));
  }

  @Test
  public void testMainGoodResults() {
    String[] arguments = {"2.0", "+", "2.0"};
    Main.main(arguments);
    assertThat(testOut.toString(), containsString("2.0 + 2.0 = 4.0"));
  }
}
