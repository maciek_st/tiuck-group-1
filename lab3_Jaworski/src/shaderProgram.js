export class ShaderProgram {
  constructor(gl, shaders) {
    this.gl = gl;
    this.shaderProgram = this.gl.createProgram();

    this.attachShaders(shaders);
    this.link();
    this.validate();
  }

  get() {
    return this.shaderProgram;
  }

  attachShaders(shaders) {
    shaders.forEach((shader) => {
      this.gl.attachShader(this.shaderProgram, shader.get());
    });
  }

  link() {
    this.gl.linkProgram(this.shaderProgram);
  }

  validate() {
    if (!this.gl.getProgramParameter(this.shaderProgram, this.gl.LINK_STATUS)) {
      throw new Error(`Shader program not valid: ${this.gl.getShaderInfoLog(this.shaderProgram)}`);
    }
  }
}
