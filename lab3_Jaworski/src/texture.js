// helper function
const isPowerOf2 = (value) => (value & (value - 1)) == 0;

const level = 0;
const width = 1;
const height = 1;
const border = 0;
const pixel = new Uint8Array([0, 0, 255, 255]);

export class Texture {
  constructor(gl, url) {
    this.gl = gl;
    this.texture = this.gl.createTexture();

    this.bind();
    this.usePlaceholder();
    this.useUrl(url);
  }

  get() {
    return this.texture;
  }

  bind() {
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
  }

  usePlaceholder() {
    const { RGBA, UNSIGNED_BYTE, TEXTURE_2D } = this.gl;

    const internalFormat = RGBA;
    const srcFormat = RGBA;
    const srcType = UNSIGNED_BYTE;

    this.gl.texImage2D(
      TEXTURE_2D,
      level,
      internalFormat,
      width,
      height,
      border,
      srcFormat,
      srcType,
      pixel
    );
  }

  useUrl(url) {
    const { RGBA, UNSIGNED_BYTE, TEXTURE_2D } = this.gl;

    const internalFormat = RGBA;
    const srcFormat = RGBA;
    const srcType = UNSIGNED_BYTE;

    const image = new Image();
    image.onload = () => {
      this.gl.bindTexture(TEXTURE_2D, this.texture);
      this.gl.texImage2D(
        TEXTURE_2D,
        level,
        internalFormat,
        srcFormat,
        srcType,
        image
      );

      // WebGL1 has different requirements for power of 2 images
      // vs non power of 2 images so check if the image is a
      // power of 2 in both dimensions.
      if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
        // Yes, it's a power of 2. Generate mips.
        this.gl.generateMipmap(TEXTURE_2D);
      } else {
        // No, it's not a power of 2. Turn of mips and set
        // wrapping to clamp to edge
        this.gl.texParameteri(TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
      }
    };
    image.src = url;
  }
}
