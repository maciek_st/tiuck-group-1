import { vsSource, fsSource } from './consts.js';
import { Renderer } from './renderer.js';
import { Shader } from './shader.js';
import { ShaderProgram } from './shaderProgram.js';
import { Buffers } from './buffers.js';
import { Texture } from './texture.js';

const canvas = document.querySelector('#glcanvas');
const gl = canvas.getContext('webgl');

init();
function init() {
  if (!gl) {
    alert('Unable to initialize WebGL');
    return;
  }

  const vertexShader = new Shader(gl, gl.VERTEX_SHADER, vsSource);
  const fragmentShader = new Shader(gl, gl.FRAGMENT_SHADER, fsSource);

  const shaders = [vertexShader, fragmentShader];
  const shaderProgram = new ShaderProgram(gl, shaders);
  const program = shaderProgram.get();

  // Collect all the info needed to use the shader program.
  // Look up which attributes our shader program is using
  // for aVertexPosition, aVertexNormal, aTextureCoord,
  // and look up uniform locations.
  const programInfo = {
    program,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(program, 'aVertexPosition'),
      vertexNormal: gl.getAttribLocation(program, 'aVertexNormal'),
      textureCoord: gl.getAttribLocation(program, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(program, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(program, 'uModelViewMatrix'),
      normalMatrix: gl.getUniformLocation(program, 'uNormalMatrix'),
      uSampler: gl.getUniformLocation(program, 'uSampler'),
    },
  };

  // Here's where we call the routine that builds all the
  // objects we'll be drawing.
  const buffers = new Buffers(gl);
  const texture = new Texture(gl, 'cubetexture.png');
  const renderer = new Renderer(gl, programInfo, buffers.get(), texture.get());

  renderer.run();
}
