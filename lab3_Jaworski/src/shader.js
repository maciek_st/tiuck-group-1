export class Shader {
  constructor(gl, type, source) {
    this.gl = gl;
    this.shader = this.gl.createShader(type);

    this.setSource(source);
    this.compile();
    this.validate();
  }

  get() {
    return this.shader;
  }

  setSource(source) {
    this.gl.shaderSource(this.shader, source);
  }

  compile() {
    this.gl.compileShader(this.shader);
  }

  validate() {
    if (!this.gl.getShaderParameter(this.shader, this.gl.COMPILE_STATUS)) {
      throw new Error(`Shader not valid: ${this.gl.getShaderInfoLog(this.shader)}`);
    }
  }
}
