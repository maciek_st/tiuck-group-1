import {
  cubePositions,
  vertexNormals,
  textureCoordinates,
  indices,
} from './consts.js';

export class Buffers {
  constructor(gl) {
    this.gl = gl;
  }

  get() {
    const {ARRAY_BUFFER, ELEMENT_ARRAY_BUFFER} = this.gl;
    
    const positionBuffer = this.create(ARRAY_BUFFER, new Float32Array(cubePositions));
    const normalBuffer = this.create(ARRAY_BUFFER, new Float32Array(vertexNormals));
    const textureCoordBuffer = this.create(ARRAY_BUFFER, new Float32Array(textureCoordinates));
    const indexBuffer = this.create(ELEMENT_ARRAY_BUFFER, new Uint16Array(indices));

    return {
      position: positionBuffer,
      normal: normalBuffer,
      textureCoord: textureCoordBuffer,
      indices: indexBuffer,
    };
  }

  create(target, data) {
    const buffer = this.gl.createBuffer();

    this.gl.bindBuffer(target, buffer);
    this.gl.bufferData(target, data, this.gl.STATIC_DRAW);

    return buffer;
  }
}
