package pl.ubicomp.mstokfisz;

/**
 * Main class being a main class of the CleanCode program It consists of only one method {@link
 * Main#main}
 *
 * @author Maciej Stokfisz
 * @version %I%
 */
public class Main {
  /**
   * Main class that prints results of print methods of various "PrintCurrentTime" Interface implementations according
   * to implementation number given as the start parameter of the program.
   *
   * @param args Array of Strings given as a program params
   */
  public static void main(String[] args) {
    PrintCurrentTime printCurrentTime;
    if (args.length != 0 && args[0].equals("1")) {
      printCurrentTime = new PrintCurrentTimeJaworski();
    } else { // Default implementation is Stokfisz's implementation
      printCurrentTime = new PrintTimeStokfisz();
    }
    System.out.println(printCurrentTime.print());
  }


}
