package Application;

import MathOperations.*;

public class Calculator {

    private InputAdapter inputAdapter;


    public static void main(String[] args) {
        final Calculator calculator = new Calculator(new InputAdapterImpl());
        calculator.run();
    }

    public Calculator(final InputAdapter inputAdapter) {
        this.inputAdapter = inputAdapter;
    }


    void run() {
        System.out.println("Application.Calculator:");
        System.out.println("1 - Addition");
        System.out.println("2 - Subtraction");
        System.out.println("3 - Multiplication");
        System.out.println("4 - Division");

        double result = doCalculation();

        System.out.println("Result: " + result);
    }

    double doCalculation() {
        System.out.println("\nChoice: ");
        int option = inputAdapter.getValue();

        System.out.println("a: ");
        int a = inputAdapter.getValue();

        System.out.println("b: ");
        int b = inputAdapter.getValue();

        IMathOperations iMathOperations = null;
        switch (option) {
            case 1:
                iMathOperations = new Addition();
                break;
            case 2:
                iMathOperations = new Subtraction();
                break;
            case 3:
                iMathOperations = new Multiplication();
                break;
            case 4:
                iMathOperations = new Division();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + option);
        }

        return iMathOperations.operation(a, b);
    }

}