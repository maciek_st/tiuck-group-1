package Application;

import java.util.Scanner;

/**
 * Created by T1 on 2020-05-18.
 */
public class InputAdapterImpl implements InputAdapter {

    private Scanner inputSteamScanner = new Scanner(System.in);

    @Override
    public int getValue() {
        return inputSteamScanner.nextInt();
    }
}
