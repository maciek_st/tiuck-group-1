package MathOperations;

public class Division implements IMathOperations {

    public double operation(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("Cannot divide by 0!");
        }
        return a / b;
    }
}
