package MathOperations;

public class Multiplication implements IMathOperations {

    public double operation(double a, double b) {
        return a*b;
    }
}
