package MathOperations;

public interface IMathOperations {

    double operation(double a, double b);
}
