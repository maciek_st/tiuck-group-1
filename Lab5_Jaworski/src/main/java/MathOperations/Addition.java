package MathOperations;

public class Addition implements IMathOperations {

    public double operation(double a, double b) {
        return a + b;
    }
}

