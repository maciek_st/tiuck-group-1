package Application;

import MathOperations.*;
import org.junit.Test;
import org.junit.Assert;

public class OperationsTest {

    @Test
    public void testAddition() {
        Addition add = new Addition();
        Assert.assertEquals(11.0, add.operation(5, 6), 0);
    }
    @Test
    public void testSubtraction() {
        Subtraction sub = new Subtraction();
        Assert.assertEquals(-1.0, sub.operation(5, 6), 0);
    }
    @Test
    public void testDivision() {
        Division div = new Division();
        Assert.assertEquals(2, div.operation(6, 3), 0);
    }
    @Test
    public void testMultiplication() {
        Multiplication mul = new Multiplication();
        Assert.assertEquals(18, mul.operation(6, 3), 0);
    }
    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        Division div = new Division();
        div.operation(6, 0);
    }

}
