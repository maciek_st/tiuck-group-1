package Application;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;


/**
 * Created by T1 on 2020-05-18.
 */
public class CalculatorTest {

    @Test
    public void testCalculatorAddition() {

        InputAdapter inputAdapterMock = new InputAdapter() {
            int counter = 0;
            int array[] = {1, 3, 4};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);

        double result = calculator.doCalculation();
        Assert.assertEquals(result, 7, 0);
    }

    @Test
    public void testCalculatorSubs() {

        InputAdapter inputAdapterMock = new InputAdapter() {

            int counter = 0;
            int array[] = {2, 3, 4};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);

        double result = calculator.doCalculation();
        Assert.assertEquals(result, -1, 0);
    }

    @Test
    public void testCalculatorMulti() {

        InputAdapter inputAdapterMock = new InputAdapter() {

            int counter = 0;
            int array[] = {3, 3, 4};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);

        double result = calculator.doCalculation();
        Assert.assertEquals(result, 12, 0);
    }

    @Test
    public void testCalculatorDivision() {

        InputAdapter inputAdapterMock = new InputAdapter() {

            int counter = 0;
            int array[] = {4, 4, 2};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);

        double result = calculator.doCalculation();
        Assert.assertEquals(result, 2, 0);
    }

    @Test(expected = IllegalStateException.class)
    public void testCalculatorErrorOption() {

        InputAdapter inputAdapterMock = new InputAdapter() {

            int counter = 0;
            int array[] = {5, 4, 2};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);
        double result = calculator.doCalculation();
    }

    @Test
    public void testCalculatorRun() {

        InputAdapter inputAdapterMock = new InputAdapter() {

            int counter = 0;
            int array[] = {4, 4, 2};

            @Override
            public int getValue() {
                return array[counter++];
            }
        };
        Calculator calculator = new Calculator(inputAdapterMock);
        calculator.run();
    }


}
